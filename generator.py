import json


from jinja2 import Environment
from jinja2 import FileSystemLoader

env = Environment(loader=FileSystemLoader("."))
template = env.get_template("cards.html")

with open("cards.json", "r") as file:
    cards = json.load(file)
    html = template.render(cards=cards)
    with open("public/cards.html", "w+") as result_file:
        result_file.write(html)
