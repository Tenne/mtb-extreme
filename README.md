# MTB Extreme

## Inhoudsopgave

[TOC]

## 1 Inleiding

Durf jij het aan om op je stoere mountainbike te stappen en het snelst naar de eindmeet te trappen? Zorg er dan zeker voor dat je de tegenstand overklast met je tactische zetten (of met je gelukt)!

## 2 Voorbereiding

### 2.1 Bordspel

#### 2.1.1 Baan

Je bent een `maker` of je bent het niet. Het bordspel dient zelf gemaakt te worden. Als eerste mag de `start` gelegd worden. Vervolgens mag iedere speler om de beurt de baan uitbreiden met één stukje tot iedereen tevree is met de baan. De baan hoeft niet alle stukken te bevatten, alsook hoeft de baan niet zo gelegd te worden zodat er rondjes kunnen gereden worden. De `start` en `finish` mogen dus ver van elkaar verwijderd liggen.

#### 2.1.2 Obstakels

Op de baan kunnen ook obstakels geplaatst worden. Om te starten mag iedere speler 1 obstakel naar keus plaatsen op de baan. Een obstakel moet pas genomen worden, als de pion landt op het vakje waar het obstakel op staat. Een obstakel vereist een bepaalde vaardigheid om te kunnen overwinnen. Indien de vereiste score niet behaald wordt, moet de speler blijven staan op het vakje van het obstaken en de volgende beurt overslaan.

Op vakjes met een obstakel, kan geen duel plaatsvinden.

| Obstakel | Vereiste score |
| ---- | ---- |
| Iglo | Behendigheid 🌕🌑🌑 en<br>Sprongkracht 🌕🌑🌑 |
| Verkeerslicht | Snelheid 🌕🌕🌕 of<br>Intellect 🌕🌕🌕 |
| Tractor | Snelheid 🌕🌕🌑 of<br>Behendigheid 🌕🌕🌑 of<br>Sprongkracht 🌕🌕🌑 |
| Honden | Snelheid 🌕🌑🌑 en<br>Behendigheid 🌕🌑🌑 en<br>Sprongkracht 🌕🌑🌑 |
| Zwaaiende dame | Intellect 🌕🌕🌕 |
| Boom | Behendigheid 🌕🌕🌕 of<br>Sprongkracht 🌕🌕🌕 |
| Vliegtuig | Snelheid 🌕🌕🌑 en<br>Behendigheid 🌕🌕🌑 |

##### Voorbeelden (Zie scores rijders verder in de handleiding)

* Een iglo staat 2 vakjes verder dan rijder 1 zijn pion. Een 3 wordt gegooid. Het obstakel mag gewoon gepasseerd worden.
* Een verkeerslicht staat op 2 vakjes van rijder 1 en er wordt 2 gegooid: een snelheid van 3 of een intellect van 3 is vereist, maar rijder 1 voldoet niet aan deze vereisten. De speler met rijder 1 zal dus een beurt moeten overslaan.
* Honden staan op 3 vakjes van rijder 2 en een 3 is geworpen, het obstakel "honden" moet dus overwonnen worden. Rijder 2 heeft zowel minstens 1 punt voor snelheid, behendigheid en sprongkracht, dus de vereisten zijn voldaan. De rijder mag naast het obstakel staan op het vakje en dient geen beurt over te slaan.


### 2.2 Pion en rijder

De tweede stap in het voorbereiden van het spel is een pion met rijderskaart kiezen. De pion heeft géén invloed op het spel, die dient lauter als pion op het speelbord.

Als de pion met bijpassende rijderskaart is gekozen, mag ook de bijpassende console erbij genomen worden (houten blokje). De rijderskaart mag nu in de houder van de console geplaatst worden.

#### 2.2.1 Console

De console heeft 4 opschriften met daarnaast 3 lege plaatsen om een score te geven:

* snelheid: wordt gebruikt voor rechte stukken in duels
* behendigheid: wordt gebruikt voor bochten in duels
* sprongkracht: wordt gebruikt voor jumps
* intellect: wordt gebruikt bij duels in samenwerking met andere statistieken.

Iedere speler mag nu 6 punten nemen en deze verdelen over de 4 categorieën om ze allemaal een score te geven van 0 tot en met 3. Afhankelijk van deze scores zal een rijder obstakels en duels al dan niet de baas kunnen. Eenmaal de 6 punten uitgedeeld zijn, mogen de punten niet meer veranderd worden (tenzij met actiekaarten). Let op dat andere spelers jouw scores niet te zien krijgen. Enkele mogelijke voorbeelden:

* rijder 1
   * snelheid: 🌕🌕🌑
   * behendigheid: 🌑🌑🌑
   * sprongkracht: 🌕🌕🌕
   * intellect: 🌕🌑🌑
* rijder 2
   * snelheid: 🌕🌑🌑
   * behendigheid: 🌕🌕🌑
   * sprongkracht: 🌕🌕🌑
   * intellect: 🌕🌑🌑
* rijder 3
   * snelheid: 🌑🌑🌑
   * behendigheid: 🌑🌑🌑
   * sprongkracht: 🌕🌕🌕
   * intellect: 🌕🌕🌕

Het is evident dat je best kijkt naar het parcours om te bepalen waarin je je punten zal investeren.

> :warning: Alle voorbeelden uit deze handleiding verwijzen steeds naar deze rijders.

### 2.3 Actiekaarten

Na het schudden van de actiekaarten, krijgt iedere speler 3 actiekaarten. Deze actiekaarten kunnen steeds gebruikt worden tijdens een beurt.  
Een actiekaart geldt tot de start van de volgende beurt, dan wordt de kaart onderaan de stapel gelegd. 
Dit wil dus zeggen dat als een speler een een actiekaart speelde en zijn beurt gedaan is en later wordt uitgedaagd voor een duel, deze kaart nog steeds van kracht is.  

Een actiekaart kan ingezet worden bij:
* Het begin van een beurt (geldt ook voor duels en obstakels tijdens die beurt)
* Het begin van een obstakel
* Het begin van een duel als uitdager
* Het begin van een duel als uitgedaagde

Een actiekaart kan voor jezelf worden ingezet, maar ook voor een andere rijder.

Een nieuwe actiekaart kan verkregen worden met de dobbelsteen of door vrijwillig een beurt over te slaan.

Het is van cruciaal belang dat er tactisch omgegaan wordt met de timing van het inzetten van de actiekaarten.  

Voorbeeld: de kaart `superbanden` (behendigheid + 1) wordt gespeeld door rijder 2 voor zichzelf. Dat betekent dat rijder 2 nu voor `behendigheid` een score van 3 heeft in plaats van een score van 2. Indien er daardoor een duel ontstaat, mag de speler het duel aangaan, en indien gewonnen wordt, een vakje verder rijden.

#### De soorten actiekaarten

Merk op dat sommige actiekaarten 1 keer voorkomen, andere 2 keer.

> :warning: Iedere actiekaart zal altijd voor 1 ronde gelden. Dat wil zeggen dat de kaart pas vervalt als de speler die de kaart speelde terug aan de beurt is, tenzij anders vermeld.

| Kaart | Beschrijving | Effect |
| ---- | ---- | ---- |
| Elektronische versnellingen | de elektronische versnellingen zorgen voor een vliegensvlugge en naadloze overgang tussen versnellingen | Snelheid + 1 |
| Superbanden | de volle superbanden rijd je nooit meer lek en kan je nog sneller door de bochten scheuren | Behendigheid + 1 |
| Hydraulisch verenpakket | dit bijzonder verenpakket zorgt voor uitzonderlijke springkrachten, the sky is the limit | Sprongkracht + 1 |
| Boordcomputer met AI | de boordcomputer zorgt voor de beste tactische beslissingen dankzij zijn artificiële intelligentie | Intellect + 1 |
| Gekke fan | een gekke fan hindert de rijder, je moet afremmen |Snelheid - 1|
| Kraaienpoten | banden worden lekgeprikt door kraaienpoten |Behendigheid - 1|
| Veerbreuk | een breuk in de veer van de voorvork gooit roet in het eten |Sprongkracht - 1|
| Biertje | iets te diep in het glas gekeken |Intellect - 1|
| Ster | een ster aan de horizon zorgt voor een duwtje in de rug |1 extra permanent punt naar keuze|
| Obstakel | (ver)plaats een obstakel naar keuze op een plaats naar keuze |permanent|
| Joker | obstakel kan worden overgeslagen |eenmalig|
| Doping | doping zorgt voor een makkelijkere tocht naar de overwinning |Snelheid + 1<br>Behendigheid + 1<br>Sprongkracht +1|
| Harpoen | trek 1 rijder die ergens voor je rijdt een plaats terug en jezelf een plaats naar voor. Let op dat de nieuwe posities van beide spelers mogelijks nieuwe obstakels of duels tot stand kunnen doen brengen. |eenmalig|
| Stok | met deze stok steek je een stokje voor de overwinning van de tegenpartij. Steek een stok in de wielen zodat de tegenpartij valt en 1 beurt dient over te slaan. |eenmalig een beurt overslaan|
| Uppercut | gegarandeerde knock-out | Win duel |
| Vraagteken | je kan hier zelf actiekaarten mee maken | Kies zelf maar |

[Digitaal overzicht](https://tenne.gitlab.io/mtb-extreme/cards.html)

### 2.4 Dobbelsteen

Met de dobbelsteen kan maximaal 5 gegooid worden. Gooi je het vlak met de `A`, mag je een actiekaart nemen en een willekeurig aantal vakjes naar voor gaan (tussen 0 en 5).

## 3 Spelverloop

Nu iedereen klaar is om te spelen, kunnen we eindelijk van start gaan met de oudste speler (het moet niet steeds de jongste zijn). 

### 3.1 Beurtverloop

Een beurt gaat als volgt:

1. de speler gooit de dobbelsteen en speelt **eventueel** een actiekaart. Let op, er mag maximaal één actiekaart gespeeld worden per beurt. Het is dus ook toegestaan je actiekaarten te sparen en geen actiekaart te spelen.

2. de speler mag zijn/haar pion evenveel vakjes naar voor zetten als er ogen staan op de bovenste zijde van de geworpen teerling. Smijt de speler bijvoorbeeld "een 3", dan mag de pion 3 vakjes naar voor geplaatst worden.

3. Indien de speler op hetzelfde vakje landt als het vakje van een tegenspeler, zal er een onderlinge strijd (duel) plaatsvinden. Het is nu de bedoeling dat je kijkt op wat voor soort vakje er gestreden wordt. De bedoeling is dat de rijder met de hoogste score die helpt bij dat vak samen met `intellect` een vakje verder springt omdat die rijder de onderlinge strijd wint. Bijvoorbeeld:
   * rijder 2 landt op een vakje waar rijder 1 al stond. Er ontstaat nu een duel. Aangezien de statistiek `behendigheid` helpt voor bochten, kunnen we zien dat de eerste rijder 0 punten heeft voor behendigheid en 1 voor intellect. Rijder 2 heeft 2 punten voor behendigheid en 1 voor intellect. Dat geeft een score van 1 voor rijder 1, en een score van 3 voor rijder 2. Dat betekent dat rijder 2 het duel wint en dus een vakje verder mag springen.
   
   Een duel kan niet gestreden worden met een speler die op het vakje van een obstakel staat (die daarin dus niet slaagde).

In het kort: de winnaar van een duel, springt een vakje verder.
