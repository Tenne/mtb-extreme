# Brainstorm

## Doel

De speler die eerst over de eindmeet komt, die wint. Kan met etappes gespeeld worden.


## Speelbord

Het spelbord wordt zelf gemaakt door de gebruikers: iedere speler legt om de beurt een stuk baan.
Niet alle stukken moeten/hoeven gebruikt te worden.


## Verloop

1. Iedere speler kiest een fiets en een fietser. Iedere fietser kiest nu 7 sterren om uit te delen aan zijn fietser.
2. Het spelbord wordt nu gelegd. Ieder kiest een stuk van het bord en plaatst het tot zoveel stukken als gewenst.
3. De actiekaarten worden geschud en iedere speler krijg 3 actiekaarten. Deze actiekaarten mogen gebruikt worden als de speler aan de beurt is. De speler mag zoveel kaarten gebruiken hij wenst per keer.
4. De pionnen (fietsen) worden aan de start gezet, de jongste deelnemer mag starten.
## Spelers

* Elke speler heeft een pion (fiets) en een fietser (think divers (vrouw, zwart, ...)).
* Ieder character (fietser) heeft 4 statistieken:
   * snelheid (rechte stukken)
   * behendigheid (bochten)
   * springkracht (jumps)
   * uithouding (klimmen)

## Kaarten

Er zijn kaarten die eenmalig gespeeld kunnen worden:
* Elektronische versnellingen: Speed++
* Superbanden: agility++
* Springs: jump++
* Doping: endurance++
* Kraaiepoten: banden stuk, sla beurt over
* Gekke fan: hindert een tegenspeler: speed--
* Nachtlawaai: uithouding--
* Sabbotage remmen: behendigheid--
* Bandenspanning gelost: jumps--
*
